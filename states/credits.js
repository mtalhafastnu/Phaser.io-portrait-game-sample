var credits = function(game) {};

credits.prototype = {
    init: function () {
        this.titleText = game.make.text(game.world.centerX, 100, "Game Title", {
            font: 'bold 50pt TheMinion',
            fill: '#ff8000',
            align: 'center',
            stroke: "green",
            strokeThickness : 10,
        });
        this.titleText.setShadow(3, 3, 'rgba(0,0,0,0.5)', 5);
        this.titleText.anchor.set(0.5);
        game.add.tween(this.titleText.scale).to({ x: 0.8, y: 0.8}, 700, Phaser.Easing.Linear.None, true, 500, 20, true);    
       
        this.stateText = game.make.text(game.world.centerX, game.world.centerY/2, "Credits", {
            font: 'bold 40pt TheMinion',
            fill: '#ff9f00',
            align: 'center',
            stroke: "#000000",
            strokeThickness : 5,
        });
        this.stateText.setShadow(3, 3, 'rgba(0,0,0,0.5)', 5);
        this.stateText.anchor.set(0.5);
        
        this.homeIcon = this.add.sprite(0, 0, 'icons', 99);
        this.homeIcon.inputEnabled = true;
        this.homeIcon.events.onInputUp.add(this.clickMusic, this);
        this.homeIcon.events.onInputUp.add(function () {
            game.state.start("gameMenu");   
        });
        this.optionCount = 1;
    },
    create: function () {
        game.add.existing(this.titleText);
        game.add.existing(this.stateText);
    },
    clickMusic: function(){
        clickmusic.stop();
        if(gameOptions.playSound == true){
            if (game.cache.isSoundDecoded('clickmusic')){
                clickmusic.play();
            }

        }
    },
};